Repo challenge Binar Gerardy Lucas


bila login dengan akun admin (yang isAdmin = true. dapat diubah dari database), maka menu dashboard akan tersedia.


cara test multiplayer battle

1. buka localhost:3000 dengan browser yang berbeda (contoh: player 1 dengan chrome, player 2 dengan microsoft edge).

3. coba login dengan akun yang berbeda (bila belum ada akun, dapat melakukan register).

4. bila sudah masuk, pilih enter lalu endpoint akan beralih ke /game_room. pilih PLAY untuk bermain secara offline, pilih CREATE ROOM untuk membuat room, dan pilih JOIN ROOM untuk bergabung ke room yang sudah tersedia. akun pertama akan melakukan CREATE ROOM dan akun kedua akan melakukan JOIN ROOM.

5. bila terdapat pop up, pencet tombol refresh. pop up akan hilang bila kondisi yang diinginkan sudah terpenuhi (contoh: saat baru masuk room player 1 akan mendapat pop up untuk menunggu player lain bergabung. setelah player 2 bergabung, coba kembali lakukan refresh maka pop up akan hilang dan siap bermain).