if (process.env.NODE_ENV !== 'production') {
    require('dotenv').config()
}

const express = require("express");
const app = express();
const passport = require('passport');
const flash = require('express-flash');
const session = require('express-session');
const methodOverride = require('method-override');
const {initialize} = require('./utils/passport-config');
const { user_game } = require('./models');

app.set("view engine", "ejs");
app.use(express.urlencoded({ extended: true }));
app.use(express.static("public"));
app.use(express.json());
app.use(flash());
app.use(session({
    secret: process.env.SESSION_SECRET,
    resave: false,
    saveUninitialized: false,
    cookie: { maxAge: 600000 }
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(methodOverride('_method'));

const router = require("./router");
app.use(router);



initialize(
    passport, 
    async username => {    
        const userData = await user_game.findOne({
            where: {username: username},
            attributes: ['id', 'username', 'password', 'isAdmin']
        }).then(data => {
            if(data) {
                return data.dataValues;
            }
            return null;
        });

        if(userData) {
            return userData
        }
        return null
    }, id => {
        const userData = user_game.findOne({
            where: {id: id},
            attributes: ['id', 'username', 'password', 'isAdmin']
        }).then(data => {
            if(data) {
                return data.dataValues;
            }
            return null
        });

        if(userData) {
            return userData
        }
        return null;
});




app.use("/", (req, res) => {
    res.status(404);
    res.send("<h1>404</h1>");
});



const port = 3000;
app.listen(port, () => {
    console.log(`server berjalan di port ${port}`);
});

