const { user_game, user_game_biodata, user_game_history } = require('../models');
const bcrypt = require('bcrypt');



let theAdmin = "Guest";

module.exports = {
    showDashboard: (req, res) => {
        
        if(theAdmin === "Guest") {
            theAdmin = req.params.id
        }

        const isAdmin = (req.user) ? req.user.isAdmin : false;
    
        user_game.findAll({
            attributes: ['id', 'username', 'isAdmin'],
            include:[{all: true, nested: true}]
        }).then(data => {
            
            const theData = [];
            
            for (let i = 0; i < data.length; i++) {
                theData.push({
                    id: data[i].dataValues.id,
                    username: data[i].dataValues.username,
                    isAdmin: data[i].dataValues.isAdmin,
                    fullname: data[i].dataValues.usergamebiodata.dataValues.fullname,
                    city: data[i].dataValues.usergamebiodata.dataValues.city,
                    age: data[i].dataValues.usergamebiodata.dataValues.age,
                    win: data[i].dataValues.usergamehistory.dataValues.win,
                    lose: data[i].dataValues.usergamehistory.dataValues.lose
                });
            }
            if(isAdmin) {
                res.render("dashboard", {
                    title: "Dashboard",
                    style: "/css/dashboard.css",
                    admin: req.params.id,
                    data: theData
                });
            } else {
                res.redirect('/');
            }
        });
    },
    deleteData: (req, res) => {

        user_game.destroy({
            where: { id: req.body.id },
        }).then(() => {
            console.log("user_game deleted")
        });
    
        user_game_biodata.destroy({
            where: { user_id: req.body.id },
        }).then(() => {
            console.log("user_gamebiodata deleted")
        });
    
        user_game_history.destroy({
            where: { user_id: req.body.id },
        }).then(() => {
            console.log("user_game_history deleted")
            res.redirect(`/dashboard/${theAdmin}`);
        });
    },
    showEditData: (req, res) => {
    
        user_game.findOne({
            where: { username: req.params.user},
            attributes: ['id', 'username', 'password'],
            include:[{all: true, nested: true}]
        }).then(data => {
    
            const theData = {
                id: data.dataValues.id,
                username: data.dataValues.username,
                password: data.dataValues.password,
                fullname: data.dataValues.usergamebiodata.dataValues.fullname,
                city: data.dataValues.usergamebiodata.dataValues.city,
                age: data.dataValues.usergamebiodata.dataValues.age,
            }
            
            res.render("edit", {
                title: `Dashboard || Edit ${theData.username}`,
                style: "/css/edit.css",
                data: theData,
                theAdmin
            });
        });
    },
    changeData: (req, res) => {
    
        user_game.findOne({
            where: { id: req.body.id },
            attributes: ['id', 'username', 'password'],
            include:[{all: true, nested: true}]
        }).then(async data => {

            try {
                const hashedPassword = await bcrypt.hash(req.body.password, 10);

                const changeUsername = (req.body.username) ? req.body.username : data.dataValues.username
                const changePassword = (req.body.password) ? hashedPassword : data.dataValues.password
                const changeFullname = (req.body.fullname) ? req.body.fullname : data.dataValues.fullname
                const changeCity = (req.body.city) ? req.body.city : data.dataValues.city
                const changeAge = (req.body.age) ? req.body.age : data.dataValues.age

                if(req.body.username || req.body.password) {
                    const userGameQuery = { 
                        where: { id: req.body.id }
                    }
                    user_game.update({
                        username: changeUsername,
                        password: changePassword
                    }, userGameQuery).then(() => {
                        user_game.findOne({
                            where: { id: req.body.id },
                            attributes: ['username']
                        }).then(data => {
                            res.redirect(`/dashboard/edit/${data.dataValues.username}`);
                        });
                    })
                } else {
                    const userGameBiodataQuery = { 
                        where: { user_id: req.body.id }
                    }
                    user_game_biodata.update({
                        fullname: changeFullname,
                        city: changeCity,
                        age: changeAge
                    }, userGameBiodataQuery).then(() => {
                        user_game.findOne({
                            where: { id: req.body.id },
                            attributes: ['username']
                        }).then(data => {
                            res.redirect(`/dashboard/edit/${data.dataValues.username}`);
                        });
                    });
                }
            } catch {
                res.redirect(`/dashboard/edit/${data.dataValues.username}`);
            }        
        });
    }
}