const {user_game_history} = require("../models");



module.exports = {
    getData: (req, res) => {

        let scoreData = {};

        user_game_history.findOne({
            where: {user_id: req.params.id},
            attributes: ['win', 'lose']
        }).then(data => {
            scoreData = {
                win: data.dataValues.win,
                lose: data.dataValues.lose
            }

            res.status(200);
            res.render("game", {
                title: "Game Suit",
                style: "/css/game.css",
                id: req.params.id,
                scoreData
            });
        });

    },
    postData: (req, res) => {

        user_game_history.findOne({
            where: {user_id: req.params.id},
            attributes: ['win', 'lose']
        }).then(data => {

            const winResult = (req.body.result == "win") ? data.dataValues.win + 1 : data.dataValues.win;
            const loseResult = (req.body.result == "lose") ? data.dataValues.lose + 1 : data.dataValues.lose;

            const query = {
                where: {user_id: req.params.id}
            }

            user_game_history.update({
                win: winResult,
                lose: loseResult
            }, query).then(() => {
                if(req.body.go == "exit") {
                    res.redirect("/game_room");
                } else {
                    res.redirect(`/games/${req.params.id}`);
                }
            });

        });

    }
}