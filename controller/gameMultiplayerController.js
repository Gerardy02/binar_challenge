const { game_room, user_game_history, user_game } = require('../models');



module.exports = {
    index: (req, res) => {

        let isPlayerOne = false;
        let isPlayerTwo = false;

        let result = false;
        let winResult = false;
        let loseResult = false;
        let drawResult = false;

        let waitPlayer = false;
        let waitPick = false;
        let waitCalculate = false;

        let winOverlay = false;
        let loseOverlay = false;

        let userScoreHistory = {
            win: 0,
            lose: 0
        }

        game_room.findOne({
            where: { id: req.params.id }
        }).then(async data => {

            const roomData = data.dataValues

            if (data.dataValues.playerOne == req.params.user) {
                isPlayerOne = true;
                if (!data.dataValues.playerTwo) {
                    waitPlayer = true;
                } else if (!data.dataValues.playerTwoChoice && data.dataValues.playerOneChoice) {
                    waitPick = true;
                } else if (!data.dataValues.playerTwoChoice && !data.dataValues.playerOneChoice) {
                    waitPick = false;
                }

                if (data.dataValues.winPlayer == "playerOne") {
                    winResult = true;
                    result = true;
                } else if (data.dataValues.winPlayer == "playerTwo") {
                    loseResult = true;
                    result = true;
                } else if (data.dataValues.winPlayer == "draw") {
                    drawResult = true;
                    result = true;
                }

                if (data.dataValues.playerOneScore == 3) {
                    winOverlay = true;
                } else if (data.dataValues.playerTwoScore == 3) {
                    loseOverlay = true
                }
            }

            if (data.dataValues.playerTwo == req.params.user) {
                isPlayerTwo = true;
                if(!data.dataValues.playerOneChoice) {
                    waitPick = true;
                }

                if (data.dataValues.winPlayer == "playerOne") {
                    loseResult = true;
                    result = true;
                } else if (data.dataValues.winPlayer == "playerTwo") {
                    winResult = true;
                    result = true;
                } else if (data.dataValues.winPlayer == "draw") {
                    drawResult = true;
                    result = true;
                }

                if (data.dataValues.playerOneScore == 3) {
                    loseOverlay = true;
                } else if (data.dataValues.playerTwoScore == 3) {
                    winOverlay = true
                }
            }

            if (data.dataValues.playerTwoChoice && data.dataValues.playerOneChoice) {
                if (!data.dataValues.winPlayer) {
                    waitCalculate = true
                }
            }

            if (data.dataValues.playerOneScore == 3 || data.dataValues.playerTwoScore == 3) {
                const findUserId = await user_game.findOne({
                    where: { username: req.params.user }
                }).then(data => {
                    return data.dataValues.id;
                });
    
                if (findUserId) {
                    user_game_history.findOne({
                        where: { user_id: findUserId}
                    }).then(data => {
                        userScoreHistory = {
                            win: data.dataValues.win,
                            lose: data.dataValues.lose
                        }
                        res.render("game-multiplayer", {
                            title: "Game Suit || Multiplayer",
                            style: "/css/game.css",
                            thisPlayer: req.params.user,
                            roomData: roomData,
                            waitPlayer,
                            waitPick,
                            waitCalculate,
                            isPlayerOne,
                            isPlayerTwo,
                            winResult,
                            loseResult,
                            drawResult,
                            result,
                            winOverlay,
                            loseOverlay,
                            userScoreHistory
                        });
                    });
                }
            } else {
                res.render("game-multiplayer", {
                    title: "Game Suit || Multiplayer",
                    style: "/css/game.css",
                    thisPlayer: req.params.user,
                    roomData: roomData,
                    waitPlayer,
                    waitPick,
                    waitCalculate,
                    isPlayerOne,
                    isPlayerTwo,
                    winResult,
                    loseResult,
                    drawResult,
                    result,
                    winOverlay,
                    loseOverlay,
                    userScoreHistory
                });
            }
        })

    },
    postData: async (req, res, next) => {

        if (req.body.action == "refresh") {
            res.redirect(`/game_multiplayer/${req.body.id}/battle/${req.body.user}`);
        }
        
        if (req.body.action == "chooseAnswer") {
            game_room.findOne({
                where: { id: req.params.id }
            }).then(async data => {

                let playerOneChoice = data.dataValues.playerOneChoice;
                let playerTwoChoice = data.dataValues.playerTwoChoice;
    
                if (data.dataValues.playerOne == req.params.user) {
                    playerOneChoice = req.body.answer
                    res.redirect(`/game_multiplayer/${req.body.id}/battle/${req.body.user}`);
                }
    
                if(data.dataValues.playerTwo == req.params.user) {
                    playerTwoChoice = req.body.answer
                    res.redirect(`/game_multiplayer/${req.body.id}/battle/${req.body.user}`);
                }
                
                const query = { 
                    where: { id: data.dataValues.id }
                }
                const updateChoice = await game_room.update({
                    playerOneChoice: playerOneChoice,
                    playerTwoChoice: playerTwoChoice
                }, query).then(() => {
                    return true
                })
                if(data.dataValues.playerTwo == req.params.user && updateChoice) {
                    next();
                }
            });
        }

        if (req.body.action == "refreshGame") {
            game_room.findOne({
                where: { id: req.params.id }
            }).then(data => {

                if (!data.dataValues.winPlayer) {
                    res.redirect(`/game_multiplayer/${req.body.id}/battle/${req.body.user}`);
                } else {
                    const query = { 
                        where: { id: req.params.id }
                    }
                    game_room.update({
                        playerOneChoice: null,
                        playerTwoChoice: null,
                        winPlayer: null
                    }, query).then(() => {
                        res.redirect(`/game_multiplayer/${req.body.id}/battle/${req.body.user}`);
                    });
                }

            });
        }

        if (req.body.action == "exitGame") {
            const findUserId = await user_game.findOne({
                where: { username: req.params.user }
            }).then(data => {
                return data.dataValues.id;
            });

            if (findUserId) { 
                user_game_history.findOne({
                    where: { user_id: findUserId }
                }).then(data => {

                    const win = (req.body.result == "win") ? data.dataValues.win + 1 : data.dataValues.win;
                    const lose = (req.body.result == "lose") ? data.dataValues.lose + 1 : data.dataValues.lose;

                    const query = { 
                        where: { user_id: findUserId }
                    } 
                    user_game_history.update({
                        win: win,
                        lose: lose
                    }, query).then(() => {
                        return true;
                    });
                });
            }

            try {
                game_room.destroy({
                    where: { id: req.params.id }
                }).then(() => {
                    res.redirect("/game_room");
                });
            } catch {
                res.redirect("/game_room");
            }
        }

    },
    checkChoice: async (req, res) => {

        const calculating = await game_room.findOne({
            where: { id: req.params.id }
        }).then(async data => {

            const playerOneChoice = data.dataValues.playerOneChoice;
            const playerTwoChoice = data.dataValues.playerTwoChoice;

            let winPlayer = data.dataValues.winPlayer;

            if (playerOneChoice == "rock" && playerTwoChoice == "scissors") {
                winPlayer = "playerOne"
            } 
            else if (playerOneChoice == "rock" && playerTwoChoice == "paper") {
                winPlayer = "playerTwo"
            }
            else if (playerOneChoice == "rock" && playerTwoChoice == "rock") {
                winPlayer = "draw"
            }
            
            if (playerOneChoice == "scissors" && playerTwoChoice == "paper") {
                winPlayer = "playerOne"
            }
            else if (playerOneChoice == "scissors" && playerTwoChoice == "rock") {
                winPlayer = "playerTwo"
            }
            else if (playerOneChoice == "scissors" && playerTwoChoice == "scissors") {
                winPlayer = "draw"
            }
            
            if (playerOneChoice == "paper" && playerTwoChoice == "rock") {
                winPlayer = "playerOne"
            }
            else if (playerOneChoice == "paper" && playerTwoChoice == "scissors") {
                winPlayer = "playerTwo"
            }
            else if (playerOneChoice == "paper" && playerTwoChoice == "paper") {
                winPlayer = "draw"
            }
            
            const query = { 
                where: { id: req.params.id }
            }
            const storeWinPlayer = await game_room.update({
                winPlayer: winPlayer
            }, query).then(() => {
                return true
            });
            
            if (storeWinPlayer) {
                return true
            }
        });
        
        if (calculating) {
            game_room.findOne({
                where: { id: req.params.id }
            }).then(data => {
                
                const playerOneScore = (data.dataValues.winPlayer == "playerOne") ? data.dataValues.playerOneScore + 1 : data.dataValues.playerOneScore;
                const playerTwoScore = (data.dataValues.winPlayer == "playerTwo") ? data.dataValues.playerTwoScore + 1 : data.dataValues.playerTwoScore;
                
                const query = { 
                    where: { id: req.params.id }
                }
                game_room.update({
                    playerOneScore: playerOneScore,
                    playerTwoScore: playerTwoScore
                }, query).then(() => {
                    return true
                });
            });
        }

    }
}