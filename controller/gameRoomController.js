const { game_room } = require('../models');



module.exports = {
    index: (req, res) => {

        const username = (req.user) ? req.user.username : null;
        const isAdmin = (req.user) ? req.user.isAdmin : false;
        const id = (req.user) ? req.user.id : null;

        game_room.findAll().then(data => {

            const gameRoomData = [];

            data.forEach(data => {
                const numberPlayer = data.dataValues.playerTwo ? "2/2" : "1/2";
                gameRoomData.push({
                    id: data.dataValues.id,
                    roomName: data.dataValues.roomName,
                    numberPlayer: numberPlayer
                });
            });

            res.status(200);
            res.render("game-room", {
                title: "Game Room",
                style: "css/game-room.css",
                username,
                isAdmin,
                id,
                gameRoomData
            });
        })
    },
    postData: (req, res) => {
        if (req.body.action == "createRoom") {
            const roomName = (req.body.roomName == '') ? `Room ${req.body.username}` : req.body.roomName;
            
            game_room.create({
                roomName: roomName,
                playerOne: req.body.username
            }).then(data => {
                res.redirect(`/game_multiplayer/${data.dataValues.id}/battle/${data.dataValues.playerOne}`);
            });
        } else if (req.body.action == "joinRoom") {
            game_room.findOne({
                where: { id: parseInt(req.body.id) },
                attributes: ['id', 'roomName', 'playerOne', 'playerTwo']
            }).then( async data => {

                if (data.dataValues.playerTwo != null) {
                    res.redirect("/game_room");
                } else {
                    const query = { 
                        where: { id: data.dataValues.id }
                    }
                    const updateDone = await game_room.update({
                        playerTwo: req.body.username
                    }, query).then(data => {
                        return data;
                    })

                    if(updateDone) {
                        game_room.findOne({
                            where: { id: parseInt(req.body.id) },
                            attributes: ['id', 'roomName', 'playerOne', 'playerTwo']
                        }).then(data => {
                            res.redirect(`/game_multiplayer/${data.dataValues.id}/battle/${data.dataValues.playerTwo}`);
                        });
                    }
                }
            });
        }
    }
}