

module.exports = {
    index: (req, res) => {
        const username = (req.user) ? req.user.username : null;
        const isAdmin = (req.user) ? req.user.isAdmin : false;
        
        res.status(200);
        res.render("index", {
            title: "Home Page",
            style: "css/style.css",
            username,
            isAdmin
        });
    },
    forLogout: (req, res) => {
        req.logout(err => {
            if (err) { return next(err); }
            res.redirect('/login');
        });
    }
}