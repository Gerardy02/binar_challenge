const { user_game } = require('../models');
const passport = require('passport');

module.exports = {
    loginIndex: (req, res) => {
        res.render("login", {
            title: "Login",
            style: "css/login.css"
        });
    },
    loginCheck: (req, res) => {

        console.log(req.body);

        user_game.findAll().then(data => {
    
            let access = false;
            let admin = false;
            let userId;
    
            data.forEach(e => {
                if (e.dataValues.username == req.body.username && e.dataValues.password == req.body.password) {
                    access = true
                    userId = e.dataValues.id
                    admin = e.dataValues.isAdmin
                    return
                }
            });

            if (access == true && !admin) {
                res.redirect(`/games/${userId}`);
            } else if (access == true && admin) {
                res.redirect(`/dashboard/${req.body.username}`);
            } else {
                res.status(403);
                res.render("login", {
                    title: "Login",
                    style: "css/login.css"
                });
            }
        });
    },
    loginAuthentication: passport.authenticate("local", {
        successRedirect: `/`,
        failureRedirect: '/login',
        failureFlash: true,
    }),
    checkAuthenticated(req, res, next) {
        if(req.isAuthenticated()) {
            return next();
        }
        res.redirect('/login');
    },
    isAuthenticated(req, res, next) {
        if(req.isAuthenticated()) {
            return res.redirect('game_room');
        }
        next();
    }
}