const { user_game, user_game_history, user_game_biodata } = require('../models');
const bcrypt = require('bcrypt');

module.exports = {
    index: (req, res) => {
        res.render("register", {
            title: "Register",
            style: "css/register.css",
            userExist: null,
            alertInfo: null
        });
    },
    registerData: (req, res) => {

        user_game.findAll().then(async data => {
    
            let userExist = null;
    
            for(let i = 0; i < data.length; i++) {
                if (data[i].username == req.body.username) {
                    userExist = data[i].username;
                }
            }
    
            let alertInfo = null;
    
            if (req.body.fullName == "") {
                alertInfo = "full name must be filled"
            } else if (req.body.city == "") {
                alertInfo = "you need to input your City"
            } else if (req.body.age == "") {
                alertInfo = 'input your age'
            } else if (userExist) {
                alertInfo = 'Username already used'
            }
            
            if (userExist || alertInfo) {
                res.status(403);
                res.render("register", {
                    title: "Register",
                    style: "css/register.css",
                    userExist,
                    alertInfo
                });
            } else {

                try {
                    const hashedPassword = await bcrypt.hash(req.body.password, 10);

                    user_game.create({
                        username: req.body.username,
                        password: hashedPassword
                    }).then(data => {
                        user_game_biodata.create({
                            user_id: data.dataValues.id,
                            fullname: req.body.fullName,
                            city: req.body.city,
                            age: req.body.age
                        })
                        user_game_history.create({
                            user_id: data.dataValues.id
                        });
                    });
                    res.redirect("/login");
                } catch {
                    res.redirect("/register");
                }
            }
        });
    }
}