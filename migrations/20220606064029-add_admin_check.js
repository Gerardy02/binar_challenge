'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    return queryInterface.addColumn(
      'user_games',
      'isAdmin',
      Sequelize.BOOLEAN
    );
  },

  async down (queryInterface, Sequelize) {
    return queryInterface.removeColumn(
      'user_games',
      'isAdmin'
    );
  }
};
