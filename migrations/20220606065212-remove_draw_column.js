'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    return queryInterface.removeColumn(
      'user_game_histories',
      'draw'
    );
  },

  async down (queryInterface, Sequelize) {
    return queryInterface.addColumn(
      'user_game_histories',
      'draw',
      Sequelize.INTEGER
    );
  }
};
