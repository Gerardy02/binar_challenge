'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.createTable('game_room', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      roomName: {
        type: Sequelize.STRING,
        allowNull: false
      },
      playerOne: {
        type: Sequelize.STRING,
        allowNull: true
      },
      playerTwo: {
        type: Sequelize.STRING,
        allowNull: true
      },
      playerOneChoice: {
        type: Sequelize.STRING,
        allowNull: true
      },
      playerTwoChoice: {
        type: Sequelize.STRING,
        allowNull: true
      },
      winPlayer: {
        type: Sequelize.STRING,
        allowNull: true
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down (queryInterface, Sequelize) {
    await queryInterface.dropTable('game_room');
  }
};
