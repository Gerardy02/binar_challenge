'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    return Promise.all([
      queryInterface.addColumn(
        'game_rooms',
        'playerOneScore',
       Sequelize.INTEGER
      ),
      queryInterface.addColumn(
        'game_rooms',
        'playerTwoScore',
       Sequelize.INTEGER
      )
    ])
  },

  async down (queryInterface, Sequelize) {
    queryInterface.removeColumn('game_rooms', 'playerOneScore'),
    queryInterface.removeColumn('game_rooms', 'playerTwoScore')
  }
};
