'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class game_room extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  game_room.init({
    roomName: DataTypes.STRING,
    playerOne: DataTypes.STRING,
    playerTwo: DataTypes.STRING,
    playerOneChoice: DataTypes.STRING,
    playerTwoChoice: DataTypes.STRING,
    playerOneScore: DataTypes.INTEGER,
    playerTwoScore: DataTypes.INTEGER,
    winPlayer: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'game_room',
  });
  return game_room;
};