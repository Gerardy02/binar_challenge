


const changeBtn = document.querySelectorAll("#change-btn");
const cancelBtn = document.querySelectorAll("#cancel-btn");


for(let i = 0; i < changeBtn.length; i++) {
    changeBtn[i].onclick = function () {
        const editOverlay = document.querySelectorAll("#edit-overlay")

        editOverlay[i].setAttribute("class", "edit-overlay-open");
    }
}

cancelBtn.forEach(e => {
    e.onclick = function() {
        const editOverlay = document.querySelectorAll("#edit-overlay");
    
        editOverlay.forEach(e => {
            e.setAttribute("class", "edit-overlay-closed")
        })
    }
});