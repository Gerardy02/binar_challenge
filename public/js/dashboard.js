

const deleteBtn = document.querySelectorAll("#delete-btn");
const cancelBtn = document.querySelectorAll("#cancel-btn");

for (let i = 0; i < deleteBtn.length; i++) {
    deleteBtn[i].onclick = function() {
        const deleteOverlay = document.querySelectorAll("#delete-overlay");
        const body = document.getElementById("body");

        const theDeleteOverlay = deleteOverlay[i]

        theDeleteOverlay.setAttribute("class", "delete-overlay-open");
        body.setAttribute("style", "overflow-y:hidden")
    }
};

cancelBtn.forEach(e => {
    e.onclick = function() {
        const deleteOverlay = document.querySelectorAll("#delete-overlay");
        const body = document.getElementById("body");
    
        deleteOverlay.forEach(e => {
            e.setAttribute("class", "delete-overlay-closed");
        });
        
        body.setAttribute("style", "overflow-y:auto")
    }
})