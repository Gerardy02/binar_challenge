


const createRoomBtn = document.getElementById("create-room-btn");

createRoomBtn.onclick = () => {
    const container = document.getElementById("container");
    const createRoomContainer = document.getElementById("create-room-container");

    container.setAttribute("class", "container-closed");
    createRoomContainer.setAttribute("class", "create-room-container");
}

const createRoomBack = document.getElementById("create-room-back-btn");

createRoomBack.onclick = () => {
    const container = document.getElementById("container");
    const createRoomContainer = document.getElementById("create-room-container");

    container.setAttribute("class", "container");
    createRoomContainer.setAttribute("class", "create-room-container-closed");
}



const joinRoomBtn = document.getElementById("join-room-btn");

joinRoomBtn.onclick = () => {
    const container = document.getElementById("container");
    const joinRoomContainer = document.getElementById("join-room-container");

    container.setAttribute("class", "container-closed");
    joinRoomContainer.setAttribute("class", "join-room-container");
}



const joinRoomBack = document.getElementById("join-room-back-btn");

joinRoomBack.onclick = () => {
    const container = document.getElementById("container");
    const joinRoomContainer = document.getElementById("join-room-container");

    container.setAttribute("class", "container");
    joinRoomContainer.setAttribute("class", "join-room-container-closed");
}







