
class Item {
    constructor() {
        this.name;
        this.beat;
    }
}

class Participant {
    constructor(name) {
        this.name = name;
    }

    generateAnswer() {
        console.log("test");
    }
}

class ScoreController {

    static playerScore = 0;
    static enemyScore = 0;

    static setPlayerScore() {
        const scorePlayer = document.getElementById("player-score");
        this.playerScore++;

        scorePlayer.innerText = this.playerScore;

    }

    static setEnemyScore() {
        const scoreEnemy = document.getElementById("enemy-score");
        this.enemyScore++;

        scoreEnemy.innerText = this.enemyScore;
    }

    static restart() {
        const scorePlayer = document.getElementById("player-score");
        const scoreEnemy = document.getElementById("enemy-score");

        this.playerScore = 0;
        this.enemyScore = 0;

        scorePlayer.innerText = this.playerScore;
        scoreEnemy.innerText = this.enemyScore;
    }
}





class Rock extends Item {
    constructor() {
        super();

        this.name = "Rock";
        this.beat = "Scissors";
    }
}

class Paper extends Item {
    constructor() {
        super();

        this.name = "Paper";
        this.beat = "Rock";
    }
}

class Scissors extends Item {
    constructor() {
        super();

        this.name = "Scissors";
        this.beat = "Paper";
    }
}



class Player extends Participant {
    constructor(name) {
        super(name);

        this.name = name;
    }

    generateAnswer() {
        return document.querySelectorAll("#player_choice");
    }
}

class Com extends Participant {
    constructor(name) {
        super(name);

        this.name = name;
    }

    generateAnswer() {
        return Math.floor(Math.random() * 3);
    }
}



gameManager();


function gameManager(canPlay) {
    canPlay = true;
    const player = new Player("player");
    const playerChoice = player.generateAnswer()

    playerChoice.forEach(data => {
        data.addEventListener("click", function () {

            if (canPlay == true) {
                setAnswer(data)
                canPlay = false;
            }           
        });
    })

    // Restart 
    const refresh = document.querySelectorAll("#refresh");

    refresh.forEach(e => {
        e.onclick = function () {
            restart();
        }
    });

    if(ScoreController.playerScore == 5) {
        const winOverlay = document.getElementById("win-overlay");
        winOverlay.setAttribute("class", "result-overlay");
    } else if (ScoreController.enemyScore == 5) {
        const loseOverlay = document.getElementById("lose-overlay");
        loseOverlay.setAttribute("class", "result-overlay");
    }
}



function setAnswer(data) {
    const choice = data.getAttribute("name");
    data.setAttribute("style", "background-color: #C4C4C4");

    const com = new Com("com");
    const comAns = com.generateAnswer();

    getAnswer(choice, comAns);
}



function getAnswer(choices, comChoice) {
    const itemArr = [];
    itemArr[0] = new Rock;
    itemArr[1] = new Paper;
    itemArr[2] = new Scissors;
    
    let playerAnswer;

    for (let i = 0; i < itemArr.length; i++) {
        if (choices == itemArr[i].name) {
            playerAnswer = itemArr[i];
        }
    }
    const enemyAnswer = itemArr[comChoice];  

    const comRoom = document.querySelectorAll("#com_choice");
    comRoom[comChoice].setAttribute("style", "background-color: #C4C4C4");

    getResult(playerAnswer, enemyAnswer)
}



function getResult(playerAns, comAns) {

    const mid = document.getElementById("mid");
    const midp = document.getElementById("inner_mid");

    if (playerAns.beat == comAns.name) {
        mid.setAttribute("class", "mid_winlose")
        midp.innerHTML = "Player 1<br>Win"
        ScoreController.setPlayerScore();
        console.log("you win");
    } else if (playerAns.name == comAns.beat) {
        mid.setAttribute("class", "mid_winlose")
        midp.innerHTML = "Com<br>Win"
        ScoreController.setEnemyScore();
        console.log("you lose");
    } else {
        mid.setAttribute("class", "mid_draw")
        midp.innerHTML = "Draw"
        console.log("draw")
    }
    setTimeout(() => {
        autoRefresh();
    }, 1200);
}



function restart() {
    ScoreController.restart();

    const winOverlay = document.getElementById("win-overlay");
    const loseOverlay = document.getElementById("lose-overlay");

    winOverlay.setAttribute("class", "result-overlay-closed");
    loseOverlay.setAttribute("class", "result-overlay-closed");
}



function autoRefresh() {
    const mid = document.getElementById("mid");
    const midp = document.getElementById("inner_mid");

    const choice = document.querySelectorAll(".choice_room");

    choice.forEach(data => {
        if (data.getAttribute("style")) {
            data.removeAttribute("style"); 
        }
    })

    mid.setAttribute("class", "mid");
    midp.innerHTML = "VS"
    const canPlay = true;
    gameManager(canPlay);
}


