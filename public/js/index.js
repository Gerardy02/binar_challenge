




const hamburger = document.getElementById("hamburger");
const exitham = document.getElementById("exitham");
const navOption = document.getElementById("nav_option");
const body = document.querySelector("body");

hamburger.onclick = function () {
    navOption.setAttribute("class", "nav_option_expand");
    body.setAttribute("style", "overflow-y: hidden;");
}

exitham.onclick = function () {
    navOption.setAttribute("class", "nav_option");
    body.setAttribute("style", "overflow-y: auto;");
}