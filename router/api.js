const express = require("express");
const api = express.Router();

const registerController = require("../controller/registerController");
const loginController = require("../controller/loginController");
const gameController = require("../controller/gameController");
const dashboardController = require("../controller/dashboardController");



api.post("register", registerController.registerData);
api.post("login", loginController.loginCheck);

api.get("games/:id", gameController.getData);
api.post("games/:id", gameController.postData);


api.get("dashboard/:id", dashboardController.showDashboard);
api.post("dashboard/:id", dashboardController.deleteData);
api.get("dashboard/edit/:user", dashboardController.showEditData);
api.post("dashboard/edit/:user", dashboardController.changeData);





module.exports = api;