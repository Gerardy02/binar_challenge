const express = require("express");
const dashboard = express.Router();

const dashboardController = require("../../controller/dashboardController");
const loginController = require('../../controller/loginController');



dashboard.get("/:id", loginController.checkAuthenticated, dashboardController.showDashboard);
dashboard.post('/:id', dashboardController.deleteData);



module.exports = dashboard;