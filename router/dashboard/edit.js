const express = require("express");
const edit = express.Router();

const dashboardController = require("../../controller/dashboardController");



edit.get("/:user", dashboardController.showEditData);
edit.post("/:user", dashboardController.changeData);



module.exports = edit;