const express = require("express");
const dashboardRouter = express.Router();

const dashboard = require("./dashboard");
const edit = require("./edit");



dashboardRouter.use("/", dashboard);
dashboardRouter.use("/edit", edit);



module.exports = dashboardRouter;