const express = require("express");
const game = express.Router();

const gameController = require("../controller/gameController");
const loginController = require("../controller/loginController");



game.get("/:id", loginController.checkAuthenticated, gameController.getData);
game.post("/:id", gameController.postData);

module.exports = game



