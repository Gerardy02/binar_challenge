const express = require("express");
const gameMultiplayer = express.Router();

const gameMultiplayerController = require("../controller/gameMultiplayerController");
const loginController = require("../controller/loginController");



gameMultiplayer.get("/:id/battle/:user", loginController.checkAuthenticated, gameMultiplayerController.index);
gameMultiplayer.post("/:id/battle/:user", gameMultiplayerController.postData, gameMultiplayerController.checkChoice);



module.exports = gameMultiplayer;