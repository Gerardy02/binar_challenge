const express = require("express");
const gameRoom = express.Router();

const gameRoomController = require("../controller/gameRoomController");
const loginController = require("../controller/loginController");



gameRoom.get("/",loginController.checkAuthenticated, gameRoomController.index);
gameRoom.post("/",loginController.checkAuthenticated, gameRoomController.postData);

module.exports = gameRoom