const express = require("express");
const home = express.Router();

const homeController = require("../controller/homeController");

home.get("/", homeController.index);
home.delete("/logout", homeController.forLogout);

module.exports = home;