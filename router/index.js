const express = require("express");
const router = express.Router();

const home = require("./home");
const login = require("./login");
const game = require("./game");
const register = require("./register");
const dashboard = require("./dashboard/index");
const gameRoom = require("./game_room");
const gameMultiplayer = require("./game_multiplayer");
const api = require("./api");



router.use("/", home);
router.use("/login", login);
router.use("/games", game);
router.use("/register", register);
router.use("/dashboard", dashboard);
router.use("/game_room", gameRoom);
router.use("/game_multiplayer", gameMultiplayer);
router.use("/api", api);



module.exports = router;