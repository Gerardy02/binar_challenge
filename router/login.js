const express = require("express");
const login = express.Router();

const loginController = require("../controller/loginController");



login.get("/", loginController.isAuthenticated, loginController.loginIndex);
login.post("/", loginController.loginAuthentication);

module.exports = login
