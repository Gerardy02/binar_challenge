const express = require("express");
const register = express.Router();

const registerController = require("../controller/registerController");

register.use(express.json());



register.get("/", registerController.index);
register.post("/", registerController.registerData);



module.exports = register;