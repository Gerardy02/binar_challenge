const fs = require("fs");

function getData() {

    const file = fs.readFileSync("./static dashboard login/account.json", "utf-8");
    return JSON.parse(file)
}
function writeData(data) {
    fs.writeFileSync("./static dashboard login/account.json", JSON.stringify(data), "utf-8");
}



function storeData(data) {
    const file = getData()
    file.push(data);

    writeData(file)
}



function checkData(userData) {
    const data = getData()

    for (let i = 0; i < data.length; i++) {
        if (data[i].username == userData.username && data[i].password == userData.password) {
            return true;
        } 
    }

    return false;
}


module.exports = {getData, storeData, checkData}

