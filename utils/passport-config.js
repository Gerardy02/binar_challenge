const LocalStrategy = require('passport-local').Strategy;
const bcrypt = require('bcrypt');



async function initialize(passport, getUserByUsername, getUserById) {
    
    const authenticateUser = async (username, password, done) => {
        const user = await getUserByUsername(username);

        if(user == null) {
            return done(null, false, { message: 'username not found' });
        }

        const bcryptCompare = await bcrypt.compare(password, user.password);

        try {
            if (bcryptCompare) {
                return done(null, user);
            } else {
                return done(null, false, { message: 'wrong password'})
            }
        } catch (e){
            return done(e);
        }
    }

   passport.use(new LocalStrategy( { usernameField: 'username' }, authenticateUser));
   passport.serializeUser((user, done) => {
        return done(null, user.id);
   });
   passport.deserializeUser(async (id, done) => {
        return done(null, await getUserById(id));
   });
}

module.exports = {initialize}